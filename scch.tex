\documentclass[english]{article}
\usepackage{graphicx}
\usepackage{url}

\begin{document}
\title{Research on Model-based Testing at AIST}
\author{{Cyrille Artho} \and {Takashi Kitamura}}
\date{}
\pagestyle{empty}
\maketitle
\thispagestyle{empty}

\section{AIST and RISEC}

The National Institute of Advanced Industrial Science and Technology (AIST)
is a public research institution that is funded by the Japanese government
and by research collaboration with industry in Japan. AIST has been
established in 2001 by combining over 40 research units at nine research
bases and several smaller sites. About 2,300 researchers and a few
thousand visiting scientists, post-doctoral fellows, students, and
technical staff work at AIST. AIST's key research fields are Materials
Science, Physics, and Chemistry, but many other areas of research are
also covered. See \url{http://www.aist.go.jp/}.

The Research Institute for Secure Systems (RISEC) was founded in 2012,
to address growing security concerns in the development of IT systems today.
RISEC carries out research in cryptography, control system security,
physical security, system design, software security, and software life-cycle
related issues. RISEC makes liaisons with the government, private
companies, and other research institutions to advance research and its
technology transfer to practitioners. The research labs of RISEC are
located in Tsukuba (near Tokyo) and Kansai (greater Osaka area).
See \url{http://www.risec.aist.go.jp/}.

\section{Research on Model-based Testing at RISEC\label{sec:intro}}

Software testing executes parts of a system under test.
Various techniques have been developed to automate testing.
In particular, \emph{model-based testing} has emerged as a fast-developing
field, where test cases are derived from an abstract model rather than
implemented directly as code.
A special model-based testing tool executes the system under test
based on input/output sequences
generated according to the model
(see Fig.~\ref{fig:mbt}).

\begin{figure}
\begin{center}
\includegraphics[scale=0.45]{mbt}
\end{center}
\caption{Model-based testing.}\label{fig:mbt}
\end{figure}

\long\def\symbolfootnote[#1]#2{\begingroup%
\def\thefootnote{\fnsymbol{footnote}}\footnote[#1]{#2}\endgroup} 

RISEC is working on several model-based testing tools, which cover
different aspects of this problem, namely generating \emph{test actions}
(sequences of commands sent to the system under test), and \emph{test data}
used as parameters in these actions.

\section{Modbat: Generating Test Sequences}

\begin{figure}
\begin{center}
\includegraphics[scale=0.48]{fsm}
\end{center}
\caption{A finite-state machine modeling component behavior.}\label{fig:fsm}
\end{figure}

Our tool \emph{Modbat} generates sequences of actions (function calls) to systems.
It is designed to make it easy to test possibly unreliable components, or
software interacting with components over an unreliable (mobile) network.
In Modbat, system behavior is described using finite-state machines
(see Fig.~\ref{fig:fsm}), which can be refined using a domain-specific
language provided by Modbat. The use of state machines provides an
intuitive, graphical base of the model, while the textual refinement
allows to specify details that cannot be expressed graphically.

Modbat generates \emph{event sequences} from that model, which call the
application programming interface (API) of the system under test. Results can
be checked in the model description itself, causing Modbat to report
the exact history of the error scenario if a defect is detected.

We have successfully applied Modbat to complex software written in Java and C.
In each of the projects, Modbat managed to generate thousands of distinct
and useful test cases within seconds. In one project we found defects that
had gone undetected through manual testing, even though much more effort
was spent on manual testing than when using Modbat. This shows that model-based
testing can reduce human effort while at the same time improving the quality of
the testing phase.

\section{FOT: Generating Test Data}

To model complex test data, we are developing a model-based and
combinatorial testing method, called Feature-oriented Testing (FOT).
In this method, test models are analyzed and represented with
``extended logic-trees''.

Fig.~\ref{fig:simpleActivateTask} shows a simplified test model for an
API function ``activateTask'' of an operating system using FOT.
Test models are analyzed by decomposing test objects with various test
concerns in a top-down manner using tree models.
Each decomposition is distinguished by \textit{and-} and
\textit{or-}decomposition, forming and-or logic trees; in the figure,
\textit{and-} and \textit{or-}decompositions are expressed
with black dots and with arcs, respectively.
Further, logic trees are extended with four kinds of constraints
called ``cross-tree constraints'' denoting additional conditions;
in Fig.~\ref{fig:simpleActivateTask} we can see two kinds of them
(``\textit{removes}'' and ``\textit{attaches}'').

\begin{figure}[h]
  \begin{center}
  \includegraphics[scale=0.65]{./simpleActivateTask3}
  \end{center}
  \caption{A simplified test model for function ``activateTask'' in FOT.}
  \label{fig:simpleActivateTask}
\end{figure}

An extended logic tree can be seen as a test model in the context of
combinatorial testing, by regarding each of \textit{or}-decomposition as a
``parameter'' and its children as ``values''.
Given that model, a set of test cases (a test suite) can be derived
by applying combinatorial testing techniques, such as pair-wise testing.
We are currently developing a tool that minimizes the number of
tests that fulfill such coverage criteria. This reduces the overall
test cost while still maintaining a good defect detection capability.
We are also developing an assistant tool for FOT, called FOT tool,
which assists users in designing test models with a GUI that automates
test case generation.

\section{Conclusions and Future Work}

Model-based testing allows a test engineer to express a variety of
test cases with a few simple models. It can greatly reduce the need
for writing test programs and save time in software development. We have
presented two model-based test tools developed at AIST.

Modbat is designed to test event-driven systems. It addresses the needs
of many projects including an ongoing project where we want to study the
reliability of cloud computing middleware.

Feature-trees provide an expressive graphical language to model data
with complex dependencies. We are currently working on efficient ways
to generate test data from such a model, and plan to use such test data
in test code derived by Modbat.

In the future AIST plans to collaborate with SCCH to develop these tools
further and find possible applications in industry in Austria. We also
plan to extend an existing case study in collaboration with SCCH and the
Johannes Kepler University in Linz, to see how expressive models can be
designed for complex algorithms and data structures.

\end{document}
