#!/usr/bin/make -f

#scch.pdf: scch.dvi
#	dvipdf scch.dvi

#scch.dvi: scch.tex
##	make -C pics
#	latex scch.tex
#	latex scch.tex

scch.pdf: scch.tex
#	make -C pics
	pdflatex scch.tex
	pdflatex scch.tex

clean:
	rm -f scch.{log,aux,dvi,ps,pdf}
#make -C pics clean
